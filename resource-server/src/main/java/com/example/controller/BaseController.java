package com.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class BaseController {

    @GetMapping("/admin")
    public String admin(Principal principal) {
        return principal.getName();
    }


    @GetMapping("/user")
    public String user(Principal principal) {
        return principal.getName();
    }
}
