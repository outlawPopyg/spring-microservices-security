package com.example.controller;

import com.example.details.CustomUserDetails;
import com.example.details.CustomUserDetailsService;
import com.example.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
public class AuthController {
    private final TokenService tokenService;
    private final AuthenticationManager authManager;
    private final CustomUserDetailsService customUserDetailsService;
    record LoginRequest(String username, String password) {}
    record TokensResponse(String access_token, String refresh_token) {}

    @PostMapping("/login")
    public TokensResponse login(@RequestBody LoginRequest request) {
        UsernamePasswordAuthenticationToken token
                = new UsernamePasswordAuthenticationToken(request.username, request.password);

        Authentication authentication = authManager.authenticate(token);

        CustomUserDetails user = (CustomUserDetails) customUserDetailsService.loadUserByUsername(request.username);
        String accessToken = tokenService.generateAccessToken(user);
        String refreshToken = tokenService.generateRefreshToken(user);

        return new TokensResponse(accessToken, refreshToken);
    }

    @GetMapping("/token/refresh")
    public ResponseEntity<TokensResponse> refreshToken(HttpServletRequest request) {
        try {
            String headerAuth = request.getHeader("Authorization");
            String refreshToken = headerAuth.substring(7, headerAuth.length());

            String email = tokenService.parseToken(refreshToken);
            CustomUserDetails user = (CustomUserDetails) customUserDetailsService.loadUserByUsername(email);
            String access = tokenService.generateAccessToken(user);
            String refresh = tokenService.generateRefreshToken(user);

            return ResponseEntity.ok(new TokensResponse(access, refresh));

        } catch (BadCredentialsException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
